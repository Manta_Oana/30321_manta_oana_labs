package Ex3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class FileSearch extends JFrame{
    public JLabel label;
    public JTextField textfield;
    public JButton buttond;

    FileSearch(){

        setTitle("Search File");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        setSize(300,350);
        setVisible(true);

        int width=80;int height = 20;

        label=new JLabel("File name:");
        label.setBounds(10, 50, width, height);
        add(label);

        textfield = new JTextField(520);
        textfield.setBounds(70,50,width, height);
        add(textfield);

        buttond= new JButton("Search");
        buttond.setBounds(10,150,width, height);
        add(buttond);
        buttond.addActionListener(new ButtonEvent());



    }

        public static void main(String[] args){
            new FileSearch();
        }

      class ButtonEvent implements ActionListener{
          public void actionPerformed(ActionEvent e) {
              Desktop desktop = Desktop.getDesktop();
              String f = FileSearch.this.textfield.getText();
              File file= new File(f);
              if(file.exists()) {
                  try {
                      desktop.open(file);
                  } catch (IOException ioException) {
                      ioException.printStackTrace();
                  }
              }
          }
      }
}

