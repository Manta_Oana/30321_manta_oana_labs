package Ex6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chronometer extends JFrame
{
    JTextField milliseconds;
    JTextField seconds;
    JTextField minutes;
    JButton start;
    JButton stop;
    JButton restart;

    int minute=0;
    int second=0;
    int millisecond=0;
    static boolean state=true;

    Chronometer(){
        setTitle("Chronometer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        display();
        setSize(400,00);
        setVisible(true);
    }

    public void display()
    {
        this.setLayout(null);

        milliseconds= new JTextField(""+millisecond);
        milliseconds.setBounds(220,50,80,20);

        minutes= new JTextField(""+minute);
        minutes.setBounds(50,50,80,20);

        seconds= new JTextField(""+second);
        seconds.setBounds(135,50,80,20);

        start = new JButton("Start");
        start.setBounds(50,20,80,20);

        stop = new JButton("Stop");
        stop.setBounds(135,20,80,20);

        restart = new JButton("Restart");
        restart.setBounds(220,20,80,20);

        start.addActionListener(new ButtonAction1());
        restart.addActionListener(new ButtonAction3());
        stop.addActionListener(new ButtonAction2());

        add(milliseconds);
        add(minutes);
        add(seconds);
        add(start);
        add(stop);
        add(restart);
        getContentPane();
    }

    public static void main(String[] args) {
        Chronometer c = new Chronometer();
    }

    class ButtonAction1 implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            state=true;

            Thread t = new Thread()
            {
                public void run() {
                    for(;;)
                        if (state == true) {
                            try
                            {
                                Thread.sleep(1);
                                if(millisecond>1000)
                                {
                                    millisecond=0;
                                    second++;
                                }
                                if(second>60)
                                {
                                    millisecond=0;
                                    second=0;
                                    minute++;
                                }
                                if(minute>60)
                                {
                                    millisecond=0;
                                    minute=0;
                                    second=0;
                                }
                                milliseconds.setText(""+millisecond);
                                millisecond++;
                                seconds.setText(""+second);
                                minutes.setText(""+minute);

                            }
                            catch (Exception e)
                            {

                            }


                        }
                        else {break;}
                }
            };
            t.start();

        }

    }

    class ButtonAction2 implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
            state = false;
        }
    }

    class ButtonAction3 implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
            state = false;
            second=0;
            minute=0;
            millisecond=0;

            seconds.setText(""+second);
            minutes.setText(""+minute);
            milliseconds.setText(""+millisecond);
        }
    }
}
