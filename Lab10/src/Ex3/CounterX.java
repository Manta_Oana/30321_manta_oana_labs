package Ex3;

import java.awt.*;

class CounterX extends Thread {
    int size;
    int count;
    TextField t;

    CounterX(int count,int s, int priority) {
        this.size=s;
        t = new TextField(String.valueOf(count));
        this.setPriority(priority);

    }

TextField getTextField(){return t;}

    public void run() {
        while(count < size) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            count++;
            t.setText(String.valueOf(count));
        }

    }
}
