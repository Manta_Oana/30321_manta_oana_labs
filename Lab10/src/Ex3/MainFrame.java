package Ex3;

import java.awt.*;
import javax.swing.*;

public class MainFrame extends JFrame{

    public MainFrame(){
        setTitle("Thread priority test.");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridLayout(6,1));
        setSize(500,500);setVisible(true);

    }

    public void addNewThreadComponent(CounterX x){

        JPanel p = new JPanel();
        p.setLayout(new FlowLayout());
        p.add(new JLabel("Counter "+ x.getName()));
        p.add(x.getTextField());
        add(p);

    }

    public static void main(String[] args) {
        MainFrame mf = new MainFrame();
        CounterX c1 = new CounterX(0,100,1);
        mf.addNewThreadComponent(c1);

        CounterX c2 = new CounterX(100,200,2);
        mf.addNewThreadComponent(c2);


        c1.run();
        c2.run();
    }
}

