package Ex4;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        List<Robot> robots = new ArrayList<>();


        for (int i = 0; i < 5; i++) {
            Robot r = new Robot((int)Math.random()*100,(int)Math.random()*100);
            r.setBounds(i * 100 + 30, 0, 20, 20);
            robots.add(r);
        }

        while (true) {
            for (Robot r : robots) {
                r.setLocation(r.getX(), r.getY() + 10);
                r.repaint();
            }

            Thread.sleep(200);
        }
    }
}