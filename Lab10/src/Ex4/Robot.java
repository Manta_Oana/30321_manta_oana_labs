package Ex4;

import javax.swing.*;
import java.awt.*;

public class Robot extends Component {
    int x;
    int y;
    //getters
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    //setters
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    //constructor
    public Robot(int x, int y) {
        this.x = x;
        this.y = y;
    }

}

