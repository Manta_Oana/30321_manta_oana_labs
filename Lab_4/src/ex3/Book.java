package ex3;

import ex2.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int Stock;

    public Book (String n, Author a, double p) {
        name=n; author=a; price=p;
    }
    public Book (String n, Author a, double p, int Stk) {
        name=n; author=a; price=p;Stock=Stk;
    }

    public Book(String n, String ray, double p) {
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int stock) {
        Stock = stock;
    }
    public String toString(){
        return (name + " by " + author.toString());
    }
}
