package ex2;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String n,String mail, char g){
        name=n;
        email=mail;gender=g;
    }
    public String getName(){
        return name;
    }
    public String getEmail(){
        return email;
    }

    public char getGender() {
        return gender;
    }
    public String toString()
    {
        return ( name + " (" + gender + ") @" + email);
    }

}
