package ex1;

public class Test_circle {
    public static void main(String[] args) {
        circle c1 = new circle(2.4);
        System.out.println("The circle has radius of "
                + c1.getRadius() + " and area of " + c1.getArea());
        circle c2= new circle();
        System.out.println("The circle has radius of "
                + c2.getRadius() + " and area of " + c2.getArea());
    }
}
