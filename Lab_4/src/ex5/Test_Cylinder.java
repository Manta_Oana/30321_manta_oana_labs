package ex5;

import ex1.circle;

public class Test_Cylinder {
    public static void main (String[] args) {
        Cylinder c1 = new Cylinder();
        System.out.println("Cylinder:"+ " radius=" + c1.getRadius()+ " height=" + c1.getHeight()+ " base area=" + c1.getArea()+ " volume=" + c1.getVolume());

        Cylinder c2 = new Cylinder(2.0);
        System.out.println("Cylinder:"
                + " radius=" + c2.getRadius()
                + " height=" + c2.getHeight()
                + " base area=" + ((circle) c2).getArea()
                + " volume=" + c2.getVolume());
    }
    }

