package ex4;

import ex2.Author;

public class Book2 {
    private String   name;
    private double   price;
    private Author[] authors;
    private int      qtyInStock = 0;

    public Book2(String n, Author[] a, double v, double p) {
        name = n;
        authors = a;
        price = p;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return this.price;
    }

    public Author[] getAuthors() {
        return this.authors;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return this.qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void printAuthors() {
        int authorNo = 0;
        for (Author a : this.authors) {
            System.out.println("("+(++authorNo)+") "+a);
        }
    }

    public String toString() {
        return "'" + name +"' by " + authors.length + " authors";
    }
}

