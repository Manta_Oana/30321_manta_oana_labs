package ex4;

import ex2.Author;

public class Test_Book2 {
    public static void main(String[] args) {
        Author[] authors = new Author[2];
        authors[0] = new Author("Ray", "Ray@gmail.com", 'm');
        authors[1] = new Author("Paul", "Paul@gmail.com", 'm');

        Book2 aBook = new Book2("Java", authors, 19.95, 1000);
        System.out.println("\nThe book: " + aBook);
        System.out.println("The authors are:");
        aBook.printAuthors();
    }
}
