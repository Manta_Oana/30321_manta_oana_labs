package package_main;
import package_departments.Department;
import package_entities.Company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;


class Main extends JFrame implements ActionListener {

    Company c = new Company("Nokia","Galati") ;

    JLabel lbl=new JLabel(c.toString());

    Font f=new Font("Times",Font.BOLD,30);
    Font f1=new Font("Times",Font.BOLD,16);
    Font f2=new Font("Times",Font.BOLD,12);

    JLabel lblid,lblname,lbldepartment;

    JTextField txtid,txtname,txtdepartment;
    JButton btnadd,btnsave,btnupdate,btndelete,btnexit;

    ResultSet rs=null;
    Connection con=null;
    Statement stmt=null;


    Main()
    {
        // this is display in a Frame titlebar.
        super("Employees Information ");
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we)
            {
                System.exit(0);
            }
        });
        // set layout to null
        setLayout(null);



        // add lbl label on form.
        add(lbl);

        // set the particular position on a screen
        lbl.setBounds(200,50,500,100);
        lbl.setHorizontalAlignment(lbl.CENTER );
        // set the font of lbl label
        lbl.setFont(f);


        // initializa all the label which are declared in the example above with its caption name
        lblid=new JLabel("ID");
        lblname=new JLabel("NAME");
        lbldepartment=new JLabel("DEPARTMENT");


        lblid.setBounds(300,140,100,20);
        lblname.setBounds(300,180,100,20);
        lbldepartment.setBounds(300,220,100,20);


        // add all the label on the frame
        add(lblid);
        add(lblname);
        add(lbldepartment);


        // set font
        lblid.setFont(f2);
        lblname.setFont(f2);
        lbldepartment.setFont(f2);


        // initialize the textfield with size
        txtid=new JTextField(15);
        txtname=new JTextField(15);
        txtdepartment=new JTextField(15);


        // set a particlar position on a screen with setbounds constructor
        txtid.setBounds(400,140,100,20);
        txtname.setBounds(400,180,100,20);
        txtdepartment.setBounds(400,220,100,20);



        // add textfield on a Frame
        add(txtid);
        add(txtname);
        add(txtdepartment);





        // initializa button with its caption
        btnadd=new JButton("Add");
        btnsave=new JButton("Save");
        btnupdate=new JButton("Update");
        btndelete=new JButton("Delete");

        // To add tooltip in the buttons
        btnadd.setToolTipText("Click this button to Add record in the Database.");
        btnsave.setToolTipText("Click this button to Save record in the Database.");
        btnupdate.setToolTipText("Click this button to Update record in the Database.");
        btndelete.setToolTipText("Click this button to Delete record in the Database.");

        // set a particular position on a Frame
        btnadd.setBounds(200,400,100,30);
        btnsave.setBounds(310,400,100,30);
        btnupdate.setBounds(420,400,100,30);
        btndelete.setBounds(530,400,100,30);

        // add button on a frame
        add(btnadd);
        add(btnsave);
        add(btndelete);
        add(btnupdate);

        // register all the button
        btnadd.addActionListener(this);
        btnsave.addActionListener(this);
        btnupdate.addActionListener(this);
        btndelete.addActionListener(this);











        btnexit=new JButton("Exit");
        btnexit.setToolTipText("Click this button to Quit Program.");
        btnexit.setBounds(360,480,100,30);
        add(btnexit);
        btnexit.addActionListener(this);




        dbOpen();
    }


    public void actionPerformed(ActionEvent ae)
    {
        try
        {

            if(ae.getActionCommand()=="Add")
            {

                txtid.setText("");
                txtname.setText("");
                txtdepartment.setText("");

            }
            if(ae.getActionCommand()=="Update")
            {


                stmt.executeUpdate("UPDATE pay1 SET name='" + txtname.getText() + "',department='" + txtdepartment.getText() + "',days='" + "");

                dbClose();
                dbOpen();
            }
            if(ae.getActionCommand()=="Delete")
            {
                stmt.executeUpdate("DELETE FROM pay1 WHERE id=" + txtid.getText() + "");
                dbClose();
                dbOpen();
            }
            if(ae.getActionCommand()=="Save")
            {

                stmt.executeUpdate("INSERT  INTO pay1 VALUES('" +txtid.getText()+ "','"+ txtname.getText() + "','" + txtdepartment.getText() + "')");



                dbClose();
                dbOpen();
            }
            if(ae.getActionCommand()=="Next")
            {
                if(rs.next())
                {
                    setText();                 			setText();
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "You are At Already Last Record", "Message", JOptionPane.ERROR_MESSAGE);
                }
            }
            if(ae.getActionCommand()=="Previous")
            {
                if(rs.previous())
                {
                    setText();
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "You Are At Already First Record", "Message", JOptionPane.ERROR_MESSAGE);
                }
            }
            if (ae.getActionCommand()=="First")
            {
                if(rs.first())
                {
                    setText();
                }
            }
            if (ae.getActionCommand()=="Last")
            {
                if(rs.last())
                {
                    setText();
                }
            }


            if(ae.getActionCommand()=="Exit")
            {
                System.exit(0);
            }



        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }



// Method to round off decimal values

    public static float Round(float Rval, int Rpl) {
        float p = (float)Math.pow(10,Rpl);
        Rval = Rval * p;
        float tmp = Math.round(Rval);
        return (float)tmp/p;
    }

    public void dbOpen()
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

            con=DriverManager.getConnection("jdbc:odbc:mydata");
            stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery("Select * from pay1");
            if(rs.next())
                setText();
        }catch(Exception e){}
    }
    public void dbClose()
    {
        try{stmt.close();
            rs.close();
            con.close();
        }catch(Exception e){}
    }
    public void setText(){
        try{
            txtid.setText(rs.getString(1));
            txtname.setText(rs.getString(2));
            txtdepartment.setText(rs.getString(3));

        }catch(Exception ex){}
    }


    public static void main(String ar[])throws Exception
    {


        Main f1=new Main();

        // set frame size
        f1.setSize(800,600);


        f1.setVisible(true);

        UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
    }
}
