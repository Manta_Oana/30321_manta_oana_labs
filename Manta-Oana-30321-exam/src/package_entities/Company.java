package package_entities;

import package_departments.Department;
import java.util.ArrayList;

public class Company {
    private String name;
    private String address;

    ArrayList<Department> departments= new ArrayList<Department>();

    public Company(String name, String address){
        this.address=address;
        this.name=name;
        this.departments= new ArrayList<>();
    }

    public void initDepartments(){

    }
}
