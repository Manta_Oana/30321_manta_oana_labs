package package_entities;

import package_actions.LunchBreak;
import package_actions.WorkDay;

public class Employee extends Person implements WorkDay, LunchBreak {
    private boolean working;
    private int departmentId;

    public void work() {
        System.out.println("Employee is working");
    }
    public void relax(){
        System.out.println("Employee is relaxing");
    }
}

