package Ex3;

public class Controller
{
    private TemperatureSensor tempSensor;
    private LightSensor lightSensor;
    private static Controller controller;

    public static Controller getInstance(){
        if(controller == null)
            controller = new Controller();
        return controller;
    }

    public void control() throws InterruptedException {

        if(tempSensor == null)
            tempSensor = new TemperatureSensor();
        if(lightSensor == null)
            lightSensor = new LightSensor();

        for(int i = 1; i <= 20; i++){
            System.out.println("temp: " + tempSensor.readValue());
            System.out.println("light: " + lightSensor.readValue());
            Thread.sleep(1000);
        }
    }
}