package Ex1;

public class Test {
    public static void main(String[] arg)
    {
        Shape[] shape = new Shape[6];
        shape[0] = new Circle(2,"red",false);
        shape[1] = new Rectangle(3,2,"blue",true);
        shape[2] = new Square(4,"green",true);
        shape[3] = new Circle();
        shape[4] = new Rectangle();
        shape[5] = new Square();

        for(int i = 0; i < 6; i++)
        {
            System.out.println(shape[i].toString() );
            System.out.println("");
        }

    }
}

