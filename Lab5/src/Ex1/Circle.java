package Ex1;

public class Circle extends Shape{
    protected double radius;

    public Circle(){
        radius=1.0;
    }

    public Circle(double r){
        radius=r;

    }
    public Circle(double radius,String color,boolean filled)
    {
        this.radius=radius;this.color=color;this.filled=filled;
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
@Override
    public double getArea(){
        return radius * radius * Math.PI;
    }
@Override
    public double getPerimeter(){
        return Math.PI*radius*radius ;
    }
@Override
    public String toString(){
        return "Circle perimeter " + getPerimeter() + " color " + this.color +
                " area "  +getArea()+" radius " +this.radius ;
    }

}
