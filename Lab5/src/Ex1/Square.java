package Ex1;

public class Square extends Rectangle{
    protected double side;
    public Square(){
    }

    public Square(double side){
        this.side=side;
    }

    public Square(double side, String color,boolean filled){
        this.side=side;
        this.color=color;
        this.filled=filled;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    public void setWidth(double side) {
        super.setWidth(side);
    }

    public void setLength(double side) {
        super.setLength(side);
    }
    public double getArea(){
        return side*side;
    }
    @Override
    public double getPerimeter(){
        return 4*side ;
    }
     public String toString(){
        return "A square " + this.side +" color "+ this.color+
                " is filled: " +this.filled + " area: "+ getArea()+
                " perimeter: " + getPerimeter();
     }
}
