package Ex2;

public class TestImage
{
    public static void main(String[] args)
    {
        ProxyImage image = new ProxyImage("SomeExercise",false);
        ProxyImage image2 = new ProxyImage("AnotherExercise",true);
        image.display();
        image2.display();
    }
}