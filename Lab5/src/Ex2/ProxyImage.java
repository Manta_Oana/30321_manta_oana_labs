package Ex2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private boolean rotate;

    public ProxyImage(String fileName, boolean rotate)
    {
        this.fileName = fileName;
        this.rotate = rotate;
    }

    @Override

    public void display()
    {
        if(!this.rotate)
        {
            if(realImage == null)
            {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
        else
        {
            if(rotatedImage == null)
            {
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }
    }
}